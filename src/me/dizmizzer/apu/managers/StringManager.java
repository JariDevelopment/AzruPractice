package me.dizmizzer.apu.managers;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.dizmizzer.apu.Main;

public class StringManager {
	
	private static File Strings;
	private static FileConfiguration Stringsconfig;

	private Main p;
	
	public StringManager(Main main) {
		this.p = main;
	}
	
	public void addStringDefaults() {
		
	}
	
	public void setupString() {
		if (!(p.getDataFolder().exists())) {
			p.getDataFolder().mkdirs();
		}
		
		Strings = new File(p.getDataFolder(), "strings.yml");
		

		if (!Strings.exists()) {
			try {
				Strings.createNewFile();
				Stringsconfig = YamlConfiguration.loadConfiguration(Strings);
				
				//Invite
				Stringsconfig.set("#This part is for /duel", " ");
				Stringsconfig.set("notonline", "{Target} is not online");
				Stringsconfig.set("notenoughargs", " Not enough arguments.");
				Stringsconfig.set("invitereceive", " {Sender} has send you a duel invite. /duel accept or /duel deny");
				Stringsconfig.set("invitesend", " You have send {Target} an invite");
				Stringsconfig.set("inviteacceptreceive", " {Target} has accepted your duel");
				Stringsconfig.set("inviteacceptsend", "you have accepted {Sender}'s duel");
				Stringsconfig.set("invitedenyreceive", "{Target} has denied your duel request");
				Stringsconfig.set("invitedenysend", " You denied {Sender}'s invite");
				Stringsconfig.set("anonqueue", "You have joined the queue");
				Stringsconfig.set("anonfound", "We have found a match for you");

				//Kits
				Stringsconfig.set("#This part is for loading/saving kits", " ");

				Stringsconfig.set("kitsave", " Succesfully saved kit");
				Stringsconfig.set("kitload", " Succesfully loaded kit");
				Stringsconfig.set("kitloadfail", "Failed to load kit. The kit might be empty");
				Stringsconfig.set("kitdelete", "Succesfully deleted the kit");
				
				//Combat
				Stringsconfig.set("#This part is for combat", " ");
				Stringsconfig.set("incombat", "You are now in combat");
				Stringsconfig.set("outofcombat", "You are no longer in combat");
				Stringsconfig.set("stillincombat", "{Target} is still in combat");


				//In Duels
				Stringsconfig.set("#This part is for after duel", " ");
				Stringsconfig.set("duelwin", "You have won the duel against {Target}");
				Stringsconfig.set("duellost", "You have lost the duel against {Target}");
				Stringsconfig.set("emptyinvite", "Nobody has sent you a request.");

				//Save Config
				Stringsconfig.save(Strings);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		Stringsconfig = YamlConfiguration.loadConfiguration(Strings);
		
		
	}
	
	public FileConfiguration getStringConfig() {
		return Stringsconfig;
	}
	
	public static void reloadStringsConfig() {
		Stringsconfig = YamlConfiguration.loadConfiguration(Strings);
	}



}
