package me.dizmizzer.apu.managers;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class SettingsManager {
	
	Plugin plugin;
	static Plugin pl;
	private File file;
	private static FileConfiguration config;
	

	private File kit;
	private FileConfiguration kitconfig;
	
	private File words;
	private static FileConfiguration wordsconfig;
	
	private File loc;
	private FileConfiguration locconfig;
	

	
	public SettingsManager(Plugin p) {
		this.plugin = p;
		if (p.getDataFolder().exists()) {
			p.getDataFolder().mkdir();
		}
		
		file = new File(p.getDataFolder(), "maps.yml");

		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		config = YamlConfiguration.loadConfiguration(file);
		
		kit = new File(p.getDataFolder(), "kits.yml");
		
		if (!kit.exists()) {
			try {
				kit.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		kitconfig = YamlConfiguration.loadConfiguration(kit);
		
		/*|----------------------------------------------------- */
		/*|														*/
		/*|													   */
		/*|													  */
		/*|------------------------------------------------- */


		
		words = new File(p.getDataFolder(), "words.yml");
		
		if (!words.exists()) {
			try {
				words.createNewFile();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		wordsconfig = YamlConfiguration.loadConfiguration(words);
		
		/*|----------------------------------------------------- */
		/*|														*/
		/*|													   */
		/*|													  */
		/*|------------------------------------------------- */

		loc = new File(p.getDataFolder(), "loc.yml");
		

		if (!loc.exists()) {
			try {
				loc.createNewFile();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		locconfig = YamlConfiguration.loadConfiguration(loc);
		
		//-----------------------------------------------------------//
		//															 //
		//				The word config 							 //
		//															 //
		//-----------------------------------------------------------//
		
	}

    

	public static FileConfiguration getConfig() {
		return config;
	}

	
	public FileConfiguration getKitConfig() {
		return kitconfig;
	}
	public FileConfiguration getWordsConfig() {
		return wordsconfig;
	}
	
	public FileConfiguration getLocConfig() {
		return locconfig;
	}

	public void saveConfig() throws IOException {
		config.save(file);
		
	}
	
	public void saveLocConfig(FileConfiguration fc) {
		try {
			if (locconfig == null) {
			}
			if (loc == null) {
			}
			fc.save(plugin.getDataFolder() + "/loc.yml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void saveKitConfig() throws IOException {
		kitconfig.save(kit);
	}
	
	public void reloadLocConfig() {
		locconfig = YamlConfiguration.loadConfiguration(loc);
	}
	

}