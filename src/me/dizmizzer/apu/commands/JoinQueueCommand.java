package me.dizmizzer.apu.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.DuelManager;
import me.dizmizzer.apu.managers.StringManager;

public class JoinQueueCommand implements CommandExecutor {


	private Main plugin;
	public JoinQueueCommand(Main main) {
		this.plugin = main;
	}
	
	String arena;
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		StringManager sm = new StringManager(plugin);
		FileConfiguration fc = sm.getStringConfig();
		if (!(sender instanceof Player)) {
			return true;
		}
		
		final Player player = (Player) sender;
		
		if (plugin.combatTime.containsKey(player)) {
			player.sendMessage(toColor(fc.getString("stillincombat").replaceAll("\\{Target} is", "You are")));
		}
		
		if (plugin.queue.contains(player)) {
			player.sendMessage(toColor(fc.getString("anoninqueuealready")));
			return true;
		}
		else {
			plugin.queue.add(player);
			player.sendMessage(toColor(fc.getString("anonqueue")));
		}
		if (plugin.queue.size() > 1 ) {
			Player enemy = plugin.queue.get(0);
			if (plugin.queue.get(0) == player) {
				enemy = plugin.queue.get(1);
			}
			else {
				enemy = plugin.queue.get(0);
			}
			final DuelManager dm = new DuelManager(plugin);
			
			arena = dm.findArena(player);
			while (arena == null) {
				if ((plugin.queue.get(0) == player || plugin.queue.get(1) == player)) {
	            	player.sendMessage(toColor(fc.getString("anonsearch")));
	            	enemy.sendMessage(toColor(fc.getString("anonsearch")));
	                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
	                    public void run() {
							arena = dm.findArena(player);
							return;
	                    }
	                }, 20);
				}
			}
			if (arena != null) {
				dm.teleportPlayers(arena, enemy, player);
            	player.sendMessage(toColor(fc.getString("anonfound")).replaceAll("\\{Player}", enemy.getDisplayName()));
            	enemy.sendMessage(toColor(fc.getString("anonfound")).replaceAll("\\{Player}", player.getDisplayName()));

				plugin.queue.remove(enemy);
				plugin.queue.remove(player);
				return true;
			}

		}
		
		
		return true;
	}
	private String toColor(String string) {

		String returnable = ChatColor.translateAlternateColorCodes('&', string);
		return returnable;
	}



}
