package me.dizmizzer.apu.commands;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.ItemMeta;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.SettingsManager;
import net.md_5.bungee.api.ChatColor;

public class RenameCommand implements CommandExecutor {
	
	private Main plugin;
	public RenameCommand(Main main) {
		this.plugin = main;
	}
	
	private ArrayList<Player> cooldown = new ArrayList<Player>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		final Player player = (Player) sender;
		if (!(player.hasPermission("rename.donor"))) {
			player.sendMessage(ChatColor.RED + "Only donators can rename their items.");
			return true;
		}
		if (cooldown.contains(player)) {
			player.sendMessage("You can only do this every 10 minutes.");
			return true;
		}
		Material iih = player.getItemInHand().getType();
		if (iih == Material.DIAMOND_SWORD|| iih == Material.WOOD_SWORD || iih == Material.GOLD_SWORD || iih == Material.STONE_SWORD || iih == Material.IRON_SWORD) {
			
			SettingsManager sma = new SettingsManager(plugin);
			FileConfiguration sm = sma.getWordsConfig();
			String adjlist = sm.getString("adj");
			String wordlist = sm.getString("words");
			String[] words = wordlist.split(",");
			String[] adj = adjlist.split(",");
			
			Random rn = new Random();
			int range = words.length;
			int rangeadj = adj.length;
			int randomNum =  rn.nextInt(rangeadj);
			int randomNum2 =  rn.nextInt(range);
			
			String word1 = adj[randomNum];
			String word2 = words[randomNum2];
			player.sendMessage(ChatColor.GREEN + "Sword renamed to " + word1 + " " + word2 + "!");
			ItemMeta im = player.getItemInHand().getItemMeta();
			im.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + word1 + " " + word2);
			player.getItemInHand().setItemMeta(im);
            cooldown.add(player);
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    public void run() {
                            cooldown.remove(player);
                    }
            }, 100);

		}
		else {
			player.sendMessage(ChatColor.RED + "You can only rename swords.");
		}
		
		return true;
		
	}

}
