package me.dizmizzer.apu.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.dizmizzer.apu.listeners.PlayerKillListener;

public class KillStreakCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ks")) {
			if (sender instanceof Player) {
				sender.sendMessage(ChatColor.RED + "Your killstreak is currently at " + ChatColor.GOLD + PlayerKillListener.killStreak.get(sender) + ChatColor.RED + ".");
			}
		}
		return true;
	}
	

}
