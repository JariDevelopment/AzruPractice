package me.dizmizzer.apu.inventories;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class Anvilinventory {
    private Inventory inv;

    private ItemStack save[]= {null, null, null, null, null, null, null, null, null};
    private ItemStack load[] = {null, null, null, null, null, null, null, null, null};
    private ItemStack del[] = {null, null, null, null, null, null, null, null, null};

	
	private Inventory CreateInventory() {
		
		inv = Bukkit.createInventory(null, 27, "Choose Kit");
		for (int i = 1; i < 10; i++) {
	    	save[i-1] = createItem(Material.STAINED_GLASS_PANE, ChatColor.AQUA + "" + ChatColor.BOLD + "Save inventory #" + i, (short) 0);
	    	load[i-1] = createItem(Material.STAINED_GLASS_PANE, ChatColor.GREEN + "" + ChatColor.BOLD + "Load inventory #" + i, (short) 5);
	    	del[i-1] = createItem(Material.STAINED_GLASS_PANE, ChatColor.RED + "" + ChatColor.BOLD + "Delete inventory #" + i, (short) 14);
		}
		
		for (int i = 0; i <9; i++) {
			inv.setItem(i, save[i]);
			inv.setItem(i + 9, load[i]);
			inv.setItem(i + 18, del[i]);
		}
		
		return inv;
	}
	
	public void openKitInventory(Player player) {
		Inventory invent = CreateInventory();
		player.openInventory(invent);
	}
	
    private ItemStack createItem(Material m, String name, short dur) {
        ItemStack i = new ItemStack(m, 1, dur);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName(name);
        i.setItemMeta(im);
        return i;

    }


}
