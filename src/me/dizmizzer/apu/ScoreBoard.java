package me.dizmizzer.apu;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class ScoreBoard {
	private HashMap<Player, Integer> kill;
	private HashMap<Player, Integer> death;

	public ScoreBoard(Main main,HashMap<Player, Integer> kill, HashMap<Player, Integer> death) {
		this.kill = kill;
		this.death = death;

	}
	public ScoreBoard() {
		
	}
	

	private static double kd;
	
	 public void setScoreboardToPlayer(Player player){
		 
		 ScoreboardManager manager = Bukkit.getScoreboardManager();
		 
		 Scoreboard board = manager.getNewScoreboard();
		 Objective obj = board.registerNewObjective("stats", "dummy");
		 obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		 obj.setDisplayName(ChatColor.GOLD + "PracticeUHC");	        
		 int kills = this.kill.get(player);
		 int deaths = this.death.get(player);
		 if (deaths == 0) {
			 kd = round((double) kills, 2);
		 }
		 else {
			 kd = round((double) kills/ (double) deaths, 2);
		 }
		 Score scoreE4 = obj.getScore(ChatColor.RESET.toString());
		 scoreE4.setScore(6);

		 Score TimeScore = obj.getScore(ChatColor.YELLOW + "Kills: " + ChatColor.GOLD + kills);
		 TimeScore.setScore(4);
		 Score scoreE45 = obj.getScore(ChatColor.RED + "============" + ChatColor.RESET.toString());
		 scoreE45.setScore(5);
		 Score scoreE = obj.getScore(ChatColor.YELLOW + "Deaths: " + ChatColor.GOLD + deaths);
		 scoreE.setScore(3);
		 Score scoreS = obj.getScore(ChatColor.YELLOW + "K/D: " + ChatColor.GOLD + kd);
		 scoreS.setScore(2);
		 Score scoreE2 = obj.getScore(ChatColor.RED + "============");
		 scoreE2.setScore(1);

		 player.setScoreboard(board);
	 }

	 public static double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();

		    BigDecimal bd = new BigDecimal(value);
		    bd = bd.setScale(places, RoundingMode.HALF_UP);
		    return bd.doubleValue();
		}


}
