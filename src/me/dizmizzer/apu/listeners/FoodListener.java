package me.dizmizzer.apu.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.dizmizzer.apu.Main;

public class FoodListener implements Listener {
	
	private Main pl;
	public FoodListener(Main main) {
		this.pl = main;
		Bukkit.getPluginManager().registerEvents(this, pl);
	}
	
	@EventHandler
	public void onPlayerEat(PlayerItemConsumeEvent e) {
		Player p = e.getPlayer();
		ItemStack i = e.getItem();
		if (!(i.hasItemMeta())) {
			return;
		}
		if (i.getItemMeta().getDisplayName().contains("Golden Head")) {
			ItemStack Ga = e.getItem();
			e.setCancelled(true);
			if (p.getFoodLevel() < 16) {
				p.setFoodLevel(p.getFoodLevel() + 4);
			}
			else {
				p.setFoodLevel(20);
			}
			int amount = p.getItemInHand().getAmount();
			if (amount == 1) {
				p.getInventory().remove(Ga);
			}
			if (amount > 1) {
			p.getItemInHand().setAmount(amount - 1);
			}
			p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 120*20, 0),true);
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 10*20, 1), true);
			return;
		}
		else {
			return;
		}
	}
	
	@EventHandler
	public void weater(WeatherChangeEvent e) {
		if (e.toWeatherState()) {
			e.setCancelled(true);
		}
	}


}
