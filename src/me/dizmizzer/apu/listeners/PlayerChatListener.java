package me.dizmizzer.apu.listeners;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.dizmizzer.apu.Main;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PlayerChatListener implements Listener {
	
	HashMap<Player, Integer> kill;
	HashMap<Player, Integer> death;
	
	public PlayerChatListener(Main main,HashMap<Player, Integer> kill, HashMap<Player, Integer> death) {
		this.kill = kill;
		this.death = death;
		Bukkit.getServer().getPluginManager().registerEvents(this, main);
	}

	
	@EventHandler
	public void playerChat(AsyncPlayerChatEvent e) {
		double kd = 0.0;
		PermissionUser user = PermissionsEx.getUser(e.getPlayer());
		e.setCancelled(true);
		String prefix = user.getPrefix();
		int kills = this.kill.get(e.getPlayer());
		int deaht = this.death.get(e.getPlayer());
		if (deaht == 0) {
			 kd = 0.0;
		}
		else {
			
			kd = round((double) kills/ (double) deaht, 2);
		}
		String killprefix = ChatColor.GRAY + "[" + ChatColor.GOLD + kd + ChatColor.GRAY + "]";
		String message = e.getMessage();
		String chat =  killprefix + "" + prefix + e.getPlayer().getName() +ChatColor.WHITE + ": " +  message;
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', chat));
	}
	
	 public static double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();

		    if (value > 0.01) {
			    BigDecimal bd = new BigDecimal(value);
			    bd = bd.setScale(places, RoundingMode.HALF_UP);
			    return bd.doubleValue();
		    }
		    else {
			    return 0;
		    }
	 }


}
