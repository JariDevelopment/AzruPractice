package me.dizmizzer.apu.listeners;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.managers.SettingsManager;

public class LeaveListener implements Listener {
	
    SettingsManager settings;

    private Main plugin;
	public LeaveListener(Main main) {
		this.plugin = main;
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
		
	}
	
	public void saveLocation(Location loc, Player p) throws IOException {
		SettingsManager sm = new SettingsManager(plugin);
		FileConfiguration fc = sm.getLocConfig();
		fc.set(p.getUniqueId().toString() + ".x", loc.getX());
		fc.set(p.getUniqueId().toString() + ".y", loc.getY());
		fc.set(p.getUniqueId().toString() + ".z", loc.getZ());

		sm.saveLocConfig(fc);
	}
	
	@EventHandler
	public void onleave(PlayerQuitEvent e) throws IOException {
		
		if (plugin.queue.contains(e.getPlayer())) {
			plugin.queue.remove(e.getPlayer());
		}
		
		if (plugin.combatTime.containsKey(e.getPlayer().getName())) {
			
			World w = e.getPlayer().getWorld();
			ItemStack[] itemlist = e.getPlayer().getInventory().getContents();
    		for (ItemStack m : itemlist) {
    			Location loc = e.getPlayer().getLocation();
    			if (m == null) {
    				
    			}
    			else {
    				w.dropItem(loc, m);
    			}
    			e.getPlayer().getInventory().clear();
    		}

		}
		saveLocation(e.getPlayer().getLocation(), e.getPlayer());
	}

}
