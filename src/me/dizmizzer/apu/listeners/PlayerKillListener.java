package me.dizmizzer.apu.listeners;

import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;

import me.dizmizzer.apu.Main;
import me.dizmizzer.apu.ScoreBoard;
import me.dizmizzer.apu.managers.ArenaManager;
import me.dizmizzer.apu.managers.ItemSerialization;
import me.dizmizzer.apu.sql.WriteJson;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.util.com.google.gson.JsonIOException;
import net.minecraft.util.com.google.gson.JsonSyntaxException;

public class PlayerKillListener implements Listener {
	
	private HashMap<Player, Integer> kill;
	private HashMap<Player, Integer> death;
	private Main plugin;
	
	public static HashMap<Player, Integer> killStreak = new HashMap<Player, Integer>();


	public PlayerKillListener(Main main,HashMap<Player, Integer> kill, HashMap<Player, Integer> death) {
		this.plugin = main;
		this.kill = kill;
		this.death = death;
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);

	}
	
	@EventHandler 
	public void playerKill(PlayerDeathEvent e) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		
		if (plugin.inGame.get(e.getEntity()) == null) {
			killStreak.put(e.getEntity(), 0);
		}
		
		if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			if (plugin.inGame.get(e.getEntity()) == null) {
			Player damager = e.getEntity().getKiller();
			Player died = e.getEntity();
			
			
			if (damager == null) {
				damager = e.getEntity();
			}
			plugin.combatTime.put(damager.getName(), 1);
			plugin.combatTime.put(died.getName(), 1);
				if (damager.getItemInHand().getItemMeta().getDisplayName() != null) {
					Bukkit.broadcastMessage(ChatColor.RED + damager.getName() + ChatColor.YELLOW +  " slayed " + ChatColor.RED + died.getName() + ChatColor.YELLOW +  " using " + ChatColor.RED + damager.getItemInHand().getItemMeta().getDisplayName() + ChatColor.YELLOW + "!");
				}
				else {
					Bukkit.broadcastMessage(ChatColor.RED + damager.getName() + ChatColor.YELLOW +  " slayed " + ChatColor.RED + died.getName() + ChatColor.YELLOW +  ".");

				}
			}
			if (plugin.inGame.get(e.getEntity()) != null) {
				Player damager = e.getEntity().getKiller();
				Player died = e.getEntity();				
				ArenaManager am = new ArenaManager(plugin, died);
				am.ArenaClean(died);

				
				String invd = ItemSerialization.toBase64(damager.getInventory());
				String invdied = ItemSerialization.toBase64(died.getInventory());
				plugin.inventories.put(damager.getName(), invd);
				plugin.inventories.put(died.getName(), invdied);

				am.available.put(plugin.parena.get(died), true);
				plugin.parena.remove(damager);
				plugin.parena.remove(died);
				plugin.inGame.remove(damager);
				plugin.inGame.remove(died);
				
				Damageable dheal = damager;
				int dhealth = (int) dheal.getHealth();
				died.sendMessage( ChatColor.RED + damager.getName() + " has defeated you with " + (dhealth/2) + "❤");
				damager.sendMessage( ChatColor.RED + "you have defeated " + died.getName() + " with " + (dhealth/2) + "❤");

				died.sendMessage(ChatColor.GOLD + "inventories:");
				damager.sendMessage(ChatColor.GOLD + "inventories:");

				
				TextComponent message = new TextComponent(damager.getName());
				message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel openinventoryof " + damager.getName()));
				message.setColor(ChatColor.GOLD);

				TextComponent message2 = new TextComponent(died.getName());
				message2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel openinventoryof " + died.getName()));
				message2.setColor(ChatColor.GOLD);
				message2.setBold(true);
				message.setBold(true);
				
				died.spigot().sendMessage(message);
				damager.spigot().sendMessage(message);
				died.spigot().sendMessage(message2);
				damager.spigot().sendMessage(message2);


			}
		}
		
		WriteJson js = new WriteJson(plugin, kill, death);	
		if (!(e.getEntity().getKiller() instanceof Player)) {
				Player player = e.getEntity().getPlayer();
				
				plugin.combatTime.put(player.getName(), 2);
				
				death.put(player, death.get(player) + 1);
				js.writeJsonDeath(player);
		}

		else {
			Player died = e.getEntity().getPlayer();
			Player murderer = e.getEntity().getKiller();
			kill.put(murderer, kill.get(murderer) + 1);
			death.put(died, death.get(died) + 1);
			plugin.combatTime.put(died.getName(), 2);
			
			plugin.combatTime.put(murderer.getName(), 2);


			js.writeJsonDeath(died);
			js.writeJsonKill(murderer);
			ItemStack GH = CreateItem(Material.GOLDEN_APPLE, ChatColor.LIGHT_PURPLE + "Golden Head");
			murderer.getInventory().addItem(GH);
			if (killStreak.get(murderer) != null) {
				killStreak.put(murderer, killStreak.get(murderer) + 1);
			}
			else {
				killStreak.put(murderer, 1);
			}
			me.dizmizzer.apu.managers.Check c = new me.dizmizzer.apu.managers.Check();
			c.CheckWin(killStreak, murderer);

		}
		ScoreBoard sb = new ScoreBoard(plugin, kill, death);
		for (Player target : getOnlinePlayers()) {
			sb.setScoreboardToPlayer(target);
		}

	}
	public static List<Player> getOnlinePlayers() {
	    List<Player> list = Lists.newArrayList();
	    for (World world : Bukkit.getWorlds()) {
	        list.addAll(world.getPlayers());
	    }
	    return Collections.unmodifiableList(list);
	}
	private ItemStack CreateItem(Material m, String n) {
		
		ItemStack i = new ItemStack(Material.GOLDEN_APPLE, 1);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(n);
		i.setItemMeta(im);
		return i;
		
	}

}
