package me.dizmizzer.apu.stats;

import java.io.File;
import java.util.HashMap;

import org.bukkit.entity.Player;

public class KillDeath {
	
	HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	HashMap<Player, Integer> deaths = new HashMap<Player, Integer>();

	
	public int getKills(Player player) {
		if (kills.containsKey(player)) {
			return kills.get(player);
		}
		else {
			kills.put(player, 0);
			return 0;
		}
	}
	
	public int getDeaths(Player player) {
		if (deaths.containsKey(player)) {
			return deaths.get(player);
		}
		else {
			deaths.put(player, 0);
			return 0;
		}
	}
	
	public void loadData(File file) {
		
	}

}
