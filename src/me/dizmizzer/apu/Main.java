package me.dizmizzer.apu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;


import com.google.common.collect.Lists;

import me.dizmizzer.apu.commands.JoinPartyCommand;
import me.dizmizzer.apu.commands.JoinQueueCommand;
import me.dizmizzer.apu.commands.KillStreakCommand;
import me.dizmizzer.apu.commands.RenameCommand;
import me.dizmizzer.apu.listeners.ClickAnvilListener;
import me.dizmizzer.apu.listeners.FoodListener;
import me.dizmizzer.apu.listeners.InventoryClickListener;
import me.dizmizzer.apu.listeners.LeaveListener;
import me.dizmizzer.apu.listeners.PlayerChatListener;
import me.dizmizzer.apu.listeners.PlayerCombatListener;
import me.dizmizzer.apu.listeners.PlayerJoinListener;
import me.dizmizzer.apu.listeners.PlayerKillListener;
import me.dizmizzer.apu.managers.SettingsManager;
import me.dizmizzer.apu.managers.StringManager;
import me.dizmizzer.apu.sql.WriteJson;

public class Main extends JavaPlugin implements Listener {
	
	public HashMap<Player, String> parena = new HashMap<Player, String>();

	public HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	public HashMap<Player, Integer> deaths  = new HashMap<Player, Integer>();
	public HashMap<Player, Player> players = new HashMap<Player, Player>();

	public HashMap<String ,String> inventories = new HashMap<String, String>();
	public HashMap<Player, Boolean> inGame = new HashMap<Player, Boolean>();

	public ArrayList<Player> queue = new ArrayList<Player>();
	public HashMap<Player, String> currentArena = new HashMap<Player, String>();
	public HashMap<String, Integer> combatTime = new HashMap<String, Integer>();
	public HashMap<String, BukkitRunnable> combatTask = new HashMap<String, BukkitRunnable>();
	

	public ArrayList<Player> notincombat = new ArrayList<Player>();
	public void onEnable() {
		WriteJson js = new WriteJson(this, kills, deaths);
		new WriteJson(this, kills, deaths);
		for (Player player : getOnlinePlayers()) {
			js.addPlayerReload(player);
		}
		new ScoreBoard(this, kills, deaths);
		new PlayerJoinListener(this, kills, deaths, notincombat);
		new SettingsManager(this);
		setupCommands();
	    new ClickAnvilListener(this);
	    new PlayerKillListener(this, kills, deaths);
	    new InventoryClickListener(this);
	    new PlayerChatListener(this, kills, deaths);
	    new PlayerCombatListener(this);
	    new FoodListener(this);
	    new LeaveListener(this);
	    StringManager sm = new StringManager(this);
	    sm.setupString();
	}
	
	static Plugin plugin;
	
	private void setupCommands() {
		this.getCommand("duel").setExecutor(new JoinPartyCommand(this));
		this.getCommand("anon").setExecutor(new JoinQueueCommand(this));
		this.getCommand("ks").setExecutor(new KillStreakCommand());
		this.getCommand("rename").setExecutor(new RenameCommand(this));
		
	}
	
	public List<Player> getOnlinePlayers() {
	    List<Player> list = Lists.newArrayList();
	    for (World world : Bukkit.getWorlds()) {
	        list.addAll(world.getPlayers());
	    }
	    return Collections.unmodifiableList(list);
	}

}
