package me.dizmizzer.apu.sql;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.dizmizzer.apu.Main;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.util.com.google.gson.JsonElement;
import net.minecraft.util.com.google.gson.JsonIOException;
import net.minecraft.util.com.google.gson.JsonObject;
import net.minecraft.util.com.google.gson.JsonParser;
import net.minecraft.util.com.google.gson.JsonSyntaxException;

public class WriteJson {
	private Main plugin;
	private HashMap<Player, Integer> kill;
	private HashMap<Player, Integer> death;
	private HashMap<Material, ArrayList<String>> itemlist = new HashMap<Material, ArrayList<String>>();
	
	public WriteJson(Main main, HashMap<Player, Integer> kill, HashMap<Player, Integer> death) {
		this.plugin = main;
		this.kill = kill;
		this.death = death;
	}
	public WriteJson(Main main) {
		this.plugin = main;
		ArrayList<String> sword = new ArrayList<String>();
		ArrayList<String> rod = new ArrayList<String>();
		ArrayList<String> bow = new ArrayList<String>();
		ArrayList<String> Steak = new ArrayList<String>();
		ArrayList<String> lava = new ArrayList<String>();
		ArrayList<String> water = new ArrayList<String>();
		ArrayList<String> gapple = new ArrayList<String>();
		ArrayList<String> paxe = new ArrayList<String>();
		ArrayList<String> axe = new ArrayList<String>();
		ArrayList<String> arrow = new ArrayList<String>();
		ArrayList<String> planks = new ArrayList<String>();
		ArrayList<String> cobble = new ArrayList<String>();

		sword.add("dia_sword");
		rod.add("rod");
		bow.add("bow");
		Steak.add("Steak");
		lava.add("lava");
		lava.add("lava2");
		water.add("water");
		water.add("water2");
		gapple.add("head");
		gapple.add("gapple");
		paxe.add("pickaxe");
		axe.add("axe");
		arrow.add("Arrow");
		planks.add("Planks");
		cobble.add("Cobble1");
		cobble.add("cobble2");
		
		itemlist.put(Material.DIAMOND_SWORD, sword);
		itemlist.put(Material.FISHING_ROD, rod);
		itemlist.put(Material.BOW, bow);
		itemlist.put(Material.COOKED_BEEF, Steak);
		itemlist.put(Material.LAVA_BUCKET, lava);
		itemlist.put(Material.WATER_BUCKET, water);
		itemlist.put(Material.COBBLESTONE, cobble);
		itemlist.put(Material.GOLDEN_APPLE, gapple);
		itemlist.put(Material.DIAMOND_PICKAXE, paxe);
		itemlist.put(Material.DIAMOND_AXE, axe);
		itemlist.put(Material.ARROW, arrow);
		itemlist.put(Material.WOOD, planks);

	}
	public void writeJsonDeath(Player player) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		JsonParser parser = new JsonParser();
		Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json"));
		int deaths = death.get(player);
		int kills = kill.get(player);
		JsonObject jsobj = (JsonObject) obj;
		JsonObject stuff = new JsonObject();
		JsonObject stats = new JsonObject();
		stats.addProperty("kills", kills);
		stats.addProperty("deaths", deaths);
		stuff.add("stats", stats);
		jsobj.add("info", stuff);
		try (FileWriter file = new FileWriter(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json")) {
			file.write(jsobj.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void writeJsonKill(Player player) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		int kills = kill.get(player);
		int deaths = death.get(player);
		JsonParser parser = new JsonParser();
		Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json"));

		JsonObject jsobj = new JsonObject();
		JsonObject stuff = (JsonObject) obj;
		JsonObject stats = new JsonObject();
		jsobj.addProperty("kills", kills);
		jsobj.addProperty("deaths", deaths);
		stats.add("stats", jsobj);  //315 147 36 // 394 119 96
		stuff.add("info", stats);
		
		try (FileWriter file = new FileWriter(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json")) {
			file.write(stuff.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void writeKit(Player player, int number) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		
		final HashMap<Integer, Material> items2 = new HashMap<Integer, Material>();

		JsonParser parser = new JsonParser();

		Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json"));

		JsonObject jsobj = (JsonObject) obj;
		JsonObject kit1 = jsobj.getAsJsonObject("kits");
		JsonObject kitItem = new JsonObject();

		for (int i = 0; i < player.getInventory().getSize(); i++) {
			if (player.getInventory().getItem(i) == null) {
				
			}
			else {
				items2.put(i, player.getInventory().getItem(i).getType());
				if (player.getInventory().getItem(i).getType() == Material.DIAMOND_SWORD) {
					ItemStack sword = player.getInventory().getItem(i);
					if (sword.getItemMeta().getDisplayName() != null) {
						kitItem.addProperty("swordname", sword.getItemMeta().getDisplayName());
					}
				}
			}
		}
		
		for (int i = 0; i < 50; i++) {
			if (items2.get(i) == null) {
				
			}
			else {
				ArrayList<String> names = itemlist.get(items2.get(i));
				kitItem.addProperty(names.get(0), i);
				names.remove(0);

			}
		}
			
		
		kit1.add("kit" + number, kitItem);
		try (FileWriter file = new FileWriter(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json")) {
			file.write(jsobj.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void addJsonUser(Player player) {
		JsonObject obj = new JsonObject();
		JsonObject stats = new JsonObject();
		JsonObject info = new JsonObject();
		obj.addProperty("kills", 0);
		obj.addProperty("deaths", 0);
		stats.add("stats", obj);
		
		JsonObject kit1 = new JsonObject();
		JsonObject kitItem = new JsonObject();
		kitItem.addProperty("dia_sword", 36 -36);
		kitItem.addProperty("rod", 37-36);
		kitItem.addProperty("bow", 38 -36);
		kitItem.addProperty("Steak", 39-36);
		kitItem.addProperty("lava", 40-36);
		kitItem.addProperty("water", 41-36);
		kitItem.addProperty("Cobble1", 42-36);
		kitItem.addProperty("head", 43-36);
		kitItem.addProperty("gapple", 44-36);
		kitItem.addProperty("pickaxe", 9);
		kitItem.addProperty("axe", 10);
		kitItem.addProperty("Planks", 18);
		kitItem.addProperty("cobble2", 11);
		kitItem.addProperty("Arrow", 19);
		kitItem.addProperty("lava2", 31);
		kitItem.addProperty("water2", 32);
		kitItem.addProperty("swordname", ChatColor.RED + "" + ChatColor.BOLD + "Diamond Sword");
		kit1.add("kit1", kitItem);
		info.add("kits", kit1);
		info.add("info", stats);
		try (FileWriter file = new FileWriter(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json")) {
			file.write(info.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public int getUserKillsData(Player player) {
        JsonParser parser = new JsonParser();
        
        
        try {
			Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json"));
			
			JsonObject jsobj = (JsonObject) obj;
			JsonObject info = jsobj.getAsJsonObject("info");
			JsonObject stats = info.getAsJsonObject("stats");
			JsonElement killss = stats.get("kills");
			return Integer.parseInt(killss.toString());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

        return 0;
	}
	public int getUserDeathsData(Player player) {
        JsonParser parser = new JsonParser();
        
        
        try {
			Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json"));
			
			JsonObject jsobj = (JsonObject) obj;
			JsonObject info = jsobj.getAsJsonObject("info");

			JsonObject stats = info.getAsJsonObject("stats");
			JsonElement Deaths = stats.get("deaths");
			return Integer.parseInt(Deaths.toString());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        return 0;
	}
	
	public void getUserInventoryData(Player player, int num) {
        JsonParser parser = new JsonParser();
        
 
        try {
			Object obj = parser.parse(new FileReader(plugin.getDataFolder().toString() + "//" + player.getUniqueId().toString() + ".json" ));
			player.getInventory().clear();
			JsonObject jsobj = (JsonObject) obj;
			
			JsonObject kitdata = jsobj.getAsJsonObject("kits");
			JsonObject kitString = kitdata.getAsJsonObject("kit" + num);
			//GetArmor
			
			
			// Get all Inventory locations
			int sword = kitString.get("dia_sword").getAsInt();
			String swordname = kitString.get("swordname").getAsString();
			int rod = kitString.get("rod").getAsInt();
			int bow = kitString.get("bow").getAsInt();
			int steak  = kitString.get("Steak").getAsInt();
			int lava = kitString.get("lava").getAsInt();
			int water = kitString.get("water").getAsInt();
			int Cobble1 = kitString.get("Cobble1").getAsInt();
			int head = kitString.get("head").getAsInt();
			int gapple = kitString.get("gapple").getAsInt();
			int pickaxe = kitString.get("pickaxe").getAsInt();
			int axe = kitString.get("axe").getAsInt();
			int planks = kitString.get("Planks").getAsInt();
			int cobble2 = kitString.get("cobble2").getAsInt();
			int arrow = kitString.get("Arrow").getAsInt();
			int lava2 = kitString.get("lava2").getAsInt();
			int water2 = kitString.get("water2").getAsInt();
			
			//Enchanted item creating
			ItemStack sw = createEnchantItem(Material.DIAMOND_SWORD, Enchantment.DAMAGE_ALL, 2, swordname);
			ItemStack b = createEnchantItem(Material.BOW, Enchantment.ARROW_DAMAGE, 3, null);
			
			//Normal ItemCreation
			ItemStack r = createItem(Material.FISHING_ROD, null, 1);
			ItemStack s = createItem(Material.COOKED_BEEF, null, 64);
			ItemStack l = createItem(Material.LAVA_BUCKET, null, 1);
			ItemStack w = createItem(Material.WATER_BUCKET, null, 1);
			ItemStack c = createItem(Material.COBBLESTONE, null, 64);
			ItemStack h = createItem(Material.GOLDEN_APPLE, ChatColor.LIGHT_PURPLE + "Golden Head", 3);
			ItemStack g = createItem(Material.GOLDEN_APPLE, null, 6);
			ItemStack pa = createItem(Material.DIAMOND_PICKAXE, null, 1);
			ItemStack a = createItem(Material.DIAMOND_AXE, null, 1);
			ItemStack p = createItem(Material.WOOD, null, 64);
			ItemStack ar = createItem(Material.ARROW, null, 64);
			
			ItemStack helm = createEnchantItem(Material.DIAMOND_HELMET, Enchantment.PROTECTION_PROJECTILE, 2, null);
			ItemStack body = createEnchantItem(Material.DIAMOND_CHESTPLATE, Enchantment.PROTECTION_ENVIRONMENTAL, 2, null);
			ItemStack leg = createEnchantItem(Material.DIAMOND_LEGGINGS, Enchantment.PROTECTION_ENVIRONMENTAL, 2, null);
			ItemStack boot = createEnchantItem(Material.DIAMOND_BOOTS, Enchantment.PROTECTION_PROJECTILE, 2, null);
			
			ItemStack[] armorset = {boot, leg, body, helm};
			//SetItems
			player.getInventory().setArmorContents(armorset);
			

			player.getInventory().setItem(sword , sw);
			player.getInventory().setItem(rod , r);
			player.getInventory().setItem(bow , b);
			player.getInventory().setItem(steak , s);
			player.getInventory().setItem(lava , l);
			player.getInventory().setItem(water , w);
			player.getInventory().setItem(Cobble1 , c);
			player.getInventory().setItem(head , h);
			player.getInventory().setItem(gapple , g);
			player.getInventory().setItem(pickaxe , pa);
			player.getInventory().setItem(axe , a);
			player.getInventory().setItem(cobble2 , c);
			player.getInventory().setItem(planks , p);
			player.getInventory().setItem(arrow, ar);
			player.getInventory().setItem(water2, w);
			player.getInventory().setItem(lava2, l);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		}
		return;
	}
	
	public String getUserArmor(Player player) {
		

		return "";
	}
	
	public void addPlayerReload(Player player) {
		int deaths = getUserDeathsData(player);
		int kills = getUserKillsData(player);
		kill.put(player, kills);
		death.put(player, deaths);
	}
	
	public ItemStack createItem(Material m, String name, int amount) {
		ItemStack i = new ItemStack(m, amount);
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(name);
		i.setItemMeta(im);

		return i;
	}
	public ItemStack createEnchantItem(Material m, Enchantment e, int level, String name) {
		ItemStack i = new ItemStack(m, 1);
		ItemMeta im = i.getItemMeta();
		im.addEnchant(e, level, true);
		im.setDisplayName(name);
		i.setItemMeta(im);
		
		return i;
	}

}
